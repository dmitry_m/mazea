package com.gamesactivity.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Vector2;
import com.gamesactivity.game.*;
import com.gamesactivity.game.screens.*;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 30.08.2014.
 */
public class Manager {
    private static Game game;
    public static Vector2 size = new Vector2();
    public static boolean freeze = false;

    public static float random(float to) {
        return (float)Math.random() * to;
    }
    public static float random(float from, float to) {
        return random(to - from) + from;
    }

    public static boolean isFreeze() {
        return freeze;
    }

    public static void freeze() {
        freeze = true;
    }

    public static void restart() {
        freeze = false;
        game.setScreen(new GameScreen(game));
    }

    public static void init(Game game) {
        Utils.init();
        Manager.game = game;
    }
}
