package com.gamesactivity.game.utils;

import com.badlogic.gdx.math.Vector3;

import java.io.Serializable;

/**
 * Created by dima on 25.09.2014.
 */
public class Position3 implements Serializable {
    public int x;
    public int y;
    public int z;

    public Position3() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Position3(Position3 pos) {
        this.x = pos.x;
        this.y = pos.y;
        this.z = pos.z;
    }

    public Position3(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Position3 sum(Position3 lpos, Position3 rpos) {
        return new Position3(lpos.x + rpos.x, lpos.y + rpos.y, lpos.z + rpos.z);
    }

    public Position3(Vector3 vec) {
        this((int)vec.x, (int)vec.y, (int)vec.z);
    }

    public void add(Position3 position) {
        x += position.x;
        y += position.y;
        z += position.z;
    }

    public Vector3 toVector3() {
        return new Vector3(x, y, z);
    }
}
