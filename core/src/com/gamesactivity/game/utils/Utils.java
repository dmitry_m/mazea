package com.gamesactivity.game.utils;

import com.badlogic.gdx.math.Vector3;
import javafx.geometry.Pos;

/**
 * Created by dima on 25.09.2014.
 */

public class Utils {
    public static final int VISION_REMAIN_DEFAULT = 4;

    public static final float MIN_BRIGHTNESS = 0.4f;
    public static final float MAX_BRIGHTNESS = 0.7f;

    public static final int CELLSIZE = 4;
    public static final float ANGLE_RIGHT = 0;
    public static final float ANGLE_DOWN = 90;
    public static final float ANGLE_LEFT = 180;
    public static final float ANGLE_UP = 270;
    public static final float ANGLE_DELTA = 90;

    public static final Position2 DIR_NONE = new Position2(0, 0);
    public static final Position2 DIR_RIGHT = new Position2(1, 0);
    public static final Position2 DIR_RIGHT_DOWN = new Position2(1, -1);
    public static final Position2 DIR_DOWN = new Position2(0, -1);
    public static final Position2 DIR_DOWN_LEFT = new Position2(-1, -1);
    public static final Position2 DIR_LEFT = new Position2(-1, 0);
    public static final Position2 DIR_LEFT_UP = new Position2(-1, 1);
    public static final Position2 DIR_UP = new Position2(0, 1);
    public static final Position2 DIR_UP_RIGHT = new Position2(1, 1);

    public static final float OBSERVING_ANGLE = 140f;
    private static final float[][] DIR_ANGLES = new float[3][3];

    private static void initDirAngles() {
        setDirAngles(DIR_NONE, 0f);
        setDirAngles(DIR_RIGHT, 0f);
        setDirAngles(DIR_RIGHT_DOWN, 45f);
        setDirAngles(DIR_DOWN, 90f);
        setDirAngles(DIR_DOWN_LEFT, 135);
        setDirAngles(DIR_LEFT, 180);
        setDirAngles(DIR_LEFT_UP, 225);
        setDirAngles(DIR_UP, 270);
        setDirAngles(DIR_UP_RIGHT, 315);
    }

    private static void setDirAngles(Position2 dir, float angle) {
        DIR_ANGLES[dir.x + 1][dir.y + 1] = angle;
    }

    public static void init() {
        initDirAngles();
    }

    public static float getDirAngle(Position2 pos) {
        return DIR_ANGLES[pos.x + 1][pos.y + 1];
    }

    public static String model(String name) {
        return "data/" + name + ".g3db";
    }

    public static Position2 conv2Cell(Position2 pos) {
        return new Position2(pos.x / 4, pos.y / 4);
    }

    public static Position2 conv2Real(Position2 pos) {
        return new Position2(pos.x * 4, pos.y * 4);
    }

    public static Vector3 conv2Cell(Vector3 pos) {
        return new Vector3(pos.x / 4, pos.y / 4, pos.z / 4);
    }

    public static Vector3 conv2Real(Vector3 pos) {
        return new Vector3(pos.x * 4, pos.y * 4, pos.z * 4);
    }
}
