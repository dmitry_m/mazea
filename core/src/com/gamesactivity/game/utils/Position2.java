package com.gamesactivity.game.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import sun.misc.CEFormatException;

import javax.rmi.CORBA.Util;
import java.io.Serializable;

/**
 * Created by dima on 25.09.2014.
 */
public class Position2 implements Serializable, Cloneable {
    public int x;
    public int y;

    public Position2() {
        this.x = 0;
        this.y = 0;
    }

    public Position2(Position2 pos) {
        this.x = pos.x;
        this.y = pos.y;
    }

    public Position2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position2(Vector2 vec) {
        this((int)vec.x, (int)vec.y);
    }


    public void add(Position2 position) {
        x += position.x;
        y += position.y;
    }

    public void scl(int scalar) {
        x *= scalar;
        y *= scalar;
    }

    public void inv() {
        x *= -1;
        y *= -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Position2) {
            final Position2 oth = (Position2)obj;
            return oth.x == x && oth.y == y;
        }
        return false;
    }

    public Vector2 toVector2() {
        return new Vector2(x, y);
    }

    public Vector3 toVector3() {
        return new Vector3(x, 0, y);
    }

    public static Position2 inv(Position2 pos) {
        Position2 result = new Position2(pos);
        result.inv();
        return result;
    }

    public static Position2 sum(Position2 lpos, Position2 rpos) {
        return new Position2(lpos.x + rpos.x, lpos.y + rpos.y);
    }

    public static Position2 sub(Position2 lpos, Position2 rpos) {
        return new Position2(lpos.x - rpos.x, lpos.y - rpos.y);
    }

    public static Position2 mul(Position2 pos, int scalar) {
        return new Position2(pos.x * scalar, pos.y * scalar);
    }

    public static Position2 left(Position2 dir) {
        return new Position2(-dir.y, dir.x);
    }

    public static Position2 right(Position2 dir) {
        return new Position2(dir.y, -dir.x);
    }
}
