package com.gamesactivity.game.level.items;

import com.gamesactivity.game.utils.Position2;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dima on 25.09.2014.
 */
public class Cell implements Serializable {
    public ArrayList<Item> items = new ArrayList<Item>(8);

    public Cell(Item... items) {
        for (Item item : items) {
            this.items.add(item);
        }
    }

    public void add(Item item) {
        items.add(item);
    }

    public boolean blocking() {
        for (Item item : items) {
            if (item.context.blocking) {
                return true;
            }
        }
        return false;
    }

    public void recalculatePositions(Position2 mapOffset) {
        for (Item item : items) {
            if (null != item) {
                item.recalculatePosition(mapOffset);
            }
        }
    }
}
