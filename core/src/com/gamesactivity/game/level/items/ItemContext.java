package com.gamesactivity.game.level.items;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.utils.Position2;

import java.io.Serializable;

/**
 * Created by dima on 10.10.2014.
 */
public class ItemContext implements Serializable {
    public String[] name;
    public Position2 position;
    public Quaternion rotation;
    public Vector3 offset;
    public Position2 areas;
    public float observeAngle;
    public boolean blocking;
    public boolean transparent;
    public boolean visible;
    public int tag;
}
