package com.gamesactivity.game.level.items;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 08.10.2014.
 */
public class ItemFactory {
    InstanceFactory instanceFactory;

    public ItemFactory(InstanceFactory instanceFactory) {
        this.instanceFactory = instanceFactory;
    }

    public Item createFloorItem(String name, Position2 position, Vector3 offset, boolean blocking) {
        Item item = new Item(name);
        item.context.position = position;
        item.context.offset = offset;
        item.context.blocking = blocking;
        item.context.transparent = true;
        return instantiateAndReturn(item);
    }

    public Item createWallItem(String name, Position2 position, Quaternion rotation, Position2 areas, float observingAngle) {
        Item item = new Item(name);
        item.context.position = position;
        item.context.rotation = rotation;
        item.context.areas = areas;
        item.context.observeAngle = observingAngle;
        return instantiateAndReturn(item);
    }

    public Item createWallItem(String name, Position2 position, Quaternion rotation, Position2 areas) {
        return createWallItem(name, position, rotation, areas, Utils.OBSERVING_ANGLE);
    }

    private Item instantiateAndReturn(Item item) {
        item.instantiate(instanceFactory);
        return item;
    }
}
