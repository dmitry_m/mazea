package com.gamesactivity.game.level.items;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

import java.io.Serializable;

/**
 * Created by dima on 25.09.2014.
 */

public class Item implements Serializable {
    public static final int DEFAULT = 0;
    public static final int DARK = 1;
    public static final int DARK_MODES = 2;

    public static final float MAX_BRIGHTNESS = 1.0f;

    // to be serialized
    public ItemContext context = new ItemContext();

    private ModelInstance[] modelInstance = new ModelInstance[DARK_MODES];
    public float brightness;
    public boolean visited;

    public Item(String name) {
        this(name, name);
    }

    public Item(String name, String darkName) {
        initName(name, darkName);

        this.context.blocking = true;
        this.context.transparent = false;
        this.context.visible = true;
        this.context.offset = new Vector3();
        this.context.rotation = new Quaternion();
        this.context.tag = 0;
        this.context.areas = new Position2();
        this.context.observeAngle = Utils.OBSERVING_ANGLE;

        this.brightness = 0f;
        this.visited = false;
    }

    private void initName(String name, String darkName) {
        context.name = new String[DARK_MODES];

        context.name[DEFAULT] = name;
        context.name[DARK] = darkName;
    }

    public void instantiate(InstanceFactory factory) {
        Vector3 realPosition = new Vector3(
                context.offset.x + Utils.CELLSIZE * context.position.x,
                context.offset.y,
                context.offset.z + Utils.CELLSIZE * context.position.y);

        modelInstance[DEFAULT] = factory.createInstance(context.name[DEFAULT], realPosition, context.rotation);
        modelInstance[DARK] = factory.createInstance(context.name[DARK], realPosition, context.rotation);
    }

    public void recalculatePosition(Position2 mapOffset) {
        Vector3 realPosition = new Vector3(
                context.offset.x + Utils.CELLSIZE * (context.position.x + mapOffset.x),
                context.offset.y,
                context.offset.z + Utils.CELLSIZE * (context.position.y + mapOffset.y));

        instance(DEFAULT).transform.setTranslation(realPosition);
        instance(DARK).transform.setTranslation(realPosition);
    }

    public ModelInstance instance(int type) {
        return modelInstance[type];
    }

    public boolean visible(Position2 from, Position2 at) {
        if (context.visible) {
            final int deltaX = at.x - from.x;
            final int deltaY = at.y - from.y;
            final int signX = (0 != deltaX ? (deltaX / Math.abs(deltaX)) : 2);
            final int signY = (0 != deltaY ? (deltaY / Math.abs(deltaY)) : 2);

            return (0 == context.areas.x || signX == context.areas.x) && (0 == context.areas.y || signY == context.areas.y);
        }
        return false;
    }

    private boolean update(float delta, boolean visited, float minDarkness, float maxDarkness, InstanceRenderer renderer) {
        this.visited = visited;

        float coeff = (this.visited ? 1.0f : -1.0f);
        float remained = (this.visited ? maxDarkness - brightness : brightness - minDarkness);

        if (remained > 0) {
            if (delta < remained) {
                brightness += coeff * delta;
                return false;
            }
            else {
                brightness += coeff * (remained + 0.001f);
                return true;
            }
        }
    }

    public boolean renderFadeIn(float delta, InstanceRenderer renderer) {
        boolean result = update(delta, false, Utils.MIN_BRIGHTNESS, Utils.MAX_BRIGHTNESS, renderer);
        renderer.render(instance(Item.DEFAULT), brightness);
        return result;
    }

    public boolean renderFadeOut(float delta, InstanceRenderer renderer) {
        boolean result = update(delta, true, Utils.MIN_BRIGHTNESS, Utils.MAX_BRIGHTNESS, renderer);
        renderer.render(instance(Item.DEFAULT), brightness);
        return result;
    }
}
