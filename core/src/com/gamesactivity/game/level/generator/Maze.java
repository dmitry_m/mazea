package com.gamesactivity.game.level.generator;

import com.gamesactivity.game.utils.Position2;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by dima on 28.09.2014.
 */
public class Maze implements Serializable {
    public static final int UNDEF = 0xDEADBEAF;
    public static final int START = 0xFF00FF00;
    public static final int END = 0xFF008800;
    public static final int HORIZ = 0xFFBB8800;
    public static final int VERT = 0xFFAA0000;
    public static final int WALL = 0xFF666666;
    public static final int TABLE = 0xFF888888;
    public static final int WAY = 0xFFAAAAAA;
    public static final int UNCHECKED = 0xFFFFFFFF;

    public Position2 start;
    public Position2 end;
    public int width;
    public int height;
    transient private BufferedImage bufferedImage;

    Maze(int width, int height) {
        this.width = width;
        this.height = height;
        this.bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setColor(new Color(UNCHECKED));
        g2d.fill(new Rectangle(0, 0, width - 1, height - 1));
        g2d.setColor(new Color(WALL));
        g2d.drawRect(0, 0, width - 1, height - 1);
        g2d.dispose();
    }

    public boolean valid(int x, int y) { return !(x < 0 || x >= width || y < 0 || y >= height); }

    public boolean valid(Position2 pos) { return !(pos.x < 0 || pos.x >= width || pos.y < 0 || pos.y >= height); }

    public boolean transparent(int x, int y) { return WALL != get(x, y); }

    public boolean transparent(Position2 pos) { return WALL != get(pos); }

    public int get(int x, int y) { return bufferedImage.getRGB(x, y); }

    public int get(Position2 pos) { return bufferedImage.getRGB(pos.x, pos.y); }

    public boolean get(int x, int y, int what) { return (what == bufferedImage.getRGB(x, y)); }

    public boolean get(Position2 pos, int what) { return (what == bufferedImage.getRGB(pos.x, pos.y)); }

    public void set(int x, int y, int what) { bufferedImage.setRGB(x, y, what); }

    public void set(Position2 pos, int what) { bufferedImage.setRGB(pos.x, pos.y, what); }

    public boolean wall(int x, int y) { return (WALL == bufferedImage.getRGB(x, y)); }

    public boolean wall(Position2 pos) { return (WALL == bufferedImage.getRGB(pos.x, pos.y)); }

    public boolean table(int x, int y) { return (TABLE == bufferedImage.getRGB(x, y)); }

    public boolean table(Position2 pos) { return (TABLE == bufferedImage.getRGB(pos.x, pos.y)); }

    public boolean way(int x, int y) { return (WAY == bufferedImage.getRGB(x, y)); }

    public boolean way(Position2 pos) { return (WAY == bufferedImage.getRGB(pos.x, pos.y)); }

    public boolean room(int x, int y) { return (HORIZ == bufferedImage.getRGB(x, y) || VERT == bufferedImage.getRGB(x, y)); }

    public boolean room(Position2 pos) { return (HORIZ == bufferedImage.getRGB(pos.x, pos.y) || VERT == bufferedImage.getRGB(pos.x, pos.y)); }

    public boolean checked(int x, int y) { return (UNCHECKED != bufferedImage.getRGB(x, y)); }

    public boolean checked(Position2 pos) { return (UNCHECKED != bufferedImage.getRGB(pos.x, pos.y)); }

    public boolean unchecked(int x, int y) { return (UNCHECKED == bufferedImage.getRGB(x, y)); }

    public boolean unchecked(Position2 pos) { return (UNCHECKED == bufferedImage.getRGB(pos.x, pos.y)); }

    public boolean border(int x, int y) { return (0 >= x || width - 1 <= x || 0 >= y || height - 1 <= y); }

    public boolean border(Position2 pos) { return border(pos.x, pos.y); }

    public void setWall(int x, int y) { bufferedImage.setRGB(x, y, WALL); }

    public void setWall(Position2 pos) { bufferedImage.setRGB(pos.x, pos.y, WALL); }

    public void setTable(int x, int y) { bufferedImage.setRGB(x, y, TABLE); }

    public void setTable(Position2 pos) { bufferedImage.setRGB(pos.x, pos.y, TABLE); }

    public void setWay(int x, int y) { bufferedImage.setRGB(x, y, WAY); }

    public void setWay(Position2 pos) { bufferedImage.setRGB(pos.x, pos.y, WAY); }

    public void setRoom(int x, int y, boolean horizontal) { bufferedImage.setRGB(x, y, (horizontal ? HORIZ : VERT)); }

    public void setRoom(Position2 pos, boolean horizontal) { bufferedImage.setRGB(pos.x, pos.y, (horizontal ? HORIZ : VERT)); }

    public void setStart(int x, int y) { bufferedImage.setRGB(x, y, START); start = new Position2(x, y); }

    public void setStart(Position2 pos) { bufferedImage.setRGB(pos.x, pos.y, START); start = new Position2(pos.x, pos.y); }

    public void setEnd(int x, int y) { bufferedImage.setRGB(x, y, END); end = new Position2(x, y); }

    public void setEnd(Position2 pos) { bufferedImage.setRGB(pos.x, pos.y, END); end = new Position2(pos.x, pos.y); }

    public BufferedImage getBufferedImage() { return bufferedImage; }

    public void rotate() {
        BufferedImage rotated = new BufferedImage(bufferedImage.getHeight(), bufferedImage.getWidth(), BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                rotated.setRGB(y, x, bufferedImage.getRGB(x, y));
            }
        }
        bufferedImage = rotated;
        start = Position2.left(start);
        end = Position2.left(end);
        width = bufferedImage.getWidth();
        height = bufferedImage.getHeight();
    }

/*
    public Position2 start;
    public Position2 end;
    public int width;
    public int height;
    transient private BufferedImage bufferedImage;
*/

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeObject(start);
        out.writeObject(end);
        out.writeInt(width);
        out.writeInt(height);
        ImageIO.write(bufferedImage, "png", out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        start = (Position2)in.readObject();
        end = (Position2)in.readObject();
        width = in.readInt();
        height = in.readInt();
        bufferedImage = ImageIO.read(in);
    }
}
