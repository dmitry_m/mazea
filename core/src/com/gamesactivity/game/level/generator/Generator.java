package com.gamesactivity.game.level.generator;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.game.level.util.MazeAndRooms;
import com.gamesactivity.game.utils.Position2;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dima on 26.09.2014.
 */
public class Generator {
    private Maze maze;
    private Maze room;
    private Map<Position2, Maze> rooms;
    private int[][] paths;
    private boolean saveImages = false;

    public MazeAndRooms generate(int size, boolean saveImages) {
        this.saveImages = saveImages;
        this.maze = new Maze(size, size);
        this.rooms = new HashMap<Position2, Maze>(size / 4);

        if (saveImages) {
            for (File file : new File("out").listFiles()) {
                file.delete();
            }
        }

        discover();
        chooseEnd();

        saveImage("map.png", maze.getBufferedImage());

        return new MazeAndRooms(maze, rooms);
    }

    private void discover() {
        Array<MazeDiscoverer> guys = new Array<MazeDiscoverer>(maze.width / 4);
        guys.add(new MazeDiscoverer(
                maze,
                Maze.WALL,
                new Position2(0, maze.height/2),
                new Position2(1,0),
                5,
                1,
                0)
        );

        while (0 != guys.size) {
            for (int i = guys.size - 1; i >= 0; --i) {
                if (guys.get(i).update(guys)) {
                    guys.removeIndex(i);
                    // --i;
                }
            }
        }

        maze.setStart(1, maze.height / 2);
    }

    private void chooseEnd() {
        paths = new int[maze.width][maze.height];
        for (int[] each : paths) {
            Arrays.fill(each, Integer.MAX_VALUE);
        }

        Array<EndInMazeDiscoverer> guys = new Array<EndInMazeDiscoverer>(maze.width / 4);
        guys.add(new EndInMazeDiscoverer(maze, new Position2(0, maze.height / 2), new Position2(1, 0), 0));

        while (0 != guys.size) {
            for (int i = guys.size - 1; i >= 0; --i) {
                if (guys.get(i).update(guys)) {
                    guys.removeIndex(i);
                    //--i;
                }
            }
        }

        Position2 pos = new Position2(1, maze.height / 2);
        Position2 point = new Position2();
        for (point.x = 0; point.x < maze.width; ++point.x) {
            for (point.y = 0; point.y < maze.width; ++point.y) {
                if (paths[point.x][point.y] != Integer.MAX_VALUE
                        && paths[pos.x][pos.y] < paths[point.x][point.y]) {
                    pos.x = point.x;
                    pos.y = point.y;
                }
                else if (maze.unchecked(point)) {
                    maze.setWall(point);
                }
            }
        }

        maze.setEnd(pos);
    }

    private void generateRoom(Position2 pos, boolean horizontal) {
        Position2 roomSize = new Position2(10 + (int)(15 * MathUtils.random(0.1f, 1.0f) * MathUtils.random(0.1f, 1.0f)),
                10 + (int)(15 * MathUtils.random(0.1f, 1.0f) * MathUtils.random(0.1f, 1.0f)));

        room = new Maze(roomSize.x, roomSize.y);

        try {
            discoverRoom(horizontal);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Room size: " + room.width + "x" + room.height + "");
            throw e;
        }

        rooms.put(pos, room);

        saveImage("room" + pos.x + "x" + pos.y + ".png", room.getBufferedImage());
        room = null;
    }

    private void discoverRoom(boolean horizontal) {
        Array<MazeDiscoverer> guys = new Array<MazeDiscoverer>(room.width / 4);

        int startY = MathUtils.random(2, room.height - 3);
        guys.add(
                new MazeDiscoverer(
                        room,
                        Maze.TABLE,
                        new Position2(0, startY),
                        new Position2(1,0),
                        20,
                        0,
                        20
                )
        );

        while (0 != guys.size) {
            for (int i = guys.size - 1; i >= 0; --i) {
                if (guys.get(i).update(guys)) {
                    guys.removeIndex(i);
                    // --i;
                }
            }
        }

        // fill unchecked
        for (int x = 0; x < room.width; ++x) {
            for (int y = 0; y < room.height; ++y) {
                if (room.unchecked(x, y)) {
                    room.setTable(x, y);
                }
            }
        }

        // fill border
        for (int x = 0; x < room.width; ++x) {
            room.setWall(x, 0);
            room.setWall(x, room.height - 1);
        }
        for (int y = 0; y < room.height; ++y) {
            room.setWall(0, y);
            room.setWall(room.width - 1, y);
        }

        // clean up way near exits
        for (int y = 1; y < room.height - 1; ++y) {
            room.setWay(room.width - 2, y);
            room.setWay(1, y);
        }

        int endY = MathUtils.random(2, room.height - 3);
        room.setEnd(room.width - 1, endY);
        room.setStart(0, startY);

        if (!horizontal) {
            room.rotate();
        }
    }

    private void saveImage(String fileName, BufferedImage bufferedImage) {
        if (saveImages) {
            try {
                File outputfile = new File("out\\" + fileName);
                ImageIO.write(bufferedImage, "png", outputfile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class MazeDiscoverer {
        Maze maze;
        int fillWia;
        int wishToTurn = 0;
        int wishToTurnCoeff = 5;
        int wishToStop = 0;
        int wishToStopCoeff = 0;
        int wishToBreak = 0;
        int wishToBreakCoeff = 0;
        boolean canStop = false;

        private Position2 pos;

        private Position2 dir;
        private Position2 leftDir;
        private Position2 rightDir;

        private Position2 left;
        private Position2 right;
        private Position2 next;

        MazeDiscoverer(Maze maze, int fillWia, Position2 pos, Position2 dir, int wishToTurnCoeff, int wishToStopCoeff, int wishToBreakCoeff) {
            this.maze = maze;
            this.fillWia = fillWia;
            this.pos = new Position2(pos);

            this.dir = new Position2(dir);
            this.leftDir = Position2.left(dir);
            this.rightDir = Position2.right(dir);

            this.wishToTurnCoeff = wishToTurnCoeff;
            this.wishToStopCoeff = wishToStopCoeff;
            this.wishToBreakCoeff = wishToBreakCoeff;
        }

        boolean update(Array<MazeDiscoverer> guys) {
            try {
                return makeStepToHappyFuture(guys);
            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Position of this great scientist: "
                        + pos.x + "x" + pos.y + " (" + maze.width + "x" + maze.height + ")"
                        + ", direction: (" + dir.x + ", " + dir.y + ")");
                throw e;
            }
        }

        private boolean makeStepToHappyFuture(Array<MazeDiscoverer> guys) {
            pos.add(dir);

            left = Position2.sum(pos, leftDir);
            right = Position2.sum(pos, rightDir);
            next = Position2.sum(pos, dir);

            if (maze.checked(pos)) {
                if (!maze.way(left)) {
                    maze.set(left, fillWia);
                }
                if (!maze.way(right)) {
                    maze.set(right, fillWia);
                }
                if (!maze.border(pos) && !maze.border(next)
                        && wishToBreak > 0 && wannaBreak()
                        && maze.way(next)
                        && maze.wall(left) && maze.way(right)) {
                    maze.setWay(pos);
                    wishToBreak = 0;
                }

                return true;
            }

            Position2 nextAndLeft = Position2.sum(next, leftDir);
            Position2 nextAndRight = Position2.sum(next, rightDir);
            if (maze.way(next) || maze.way(nextAndLeft) || maze.way(nextAndRight)) {
                maze.set(left, fillWia);
                maze.set(right, fillWia);
                maze.set(pos, fillWia);
                return true;
            }

            boolean blocked = maze.checked(next);
            boolean turned = false;

            if (maze.unchecked(left)) {
                if (blocked || wannaTurn()) {
                    guys.add(new MazeDiscoverer(maze, fillWia, pos, leftDir, wishToTurnCoeff, wishToStopCoeff, wishToBreakCoeff));
                    turned = true;
                }
                else {
                    maze.set(left, fillWia);
                }
            }
            else if (wishToBreak > 0 && wannaBreak()) {
                Position2 leftLeft = Position2.sum(left, leftDir);
                if (!maze.border(leftLeft)) {
                    if (maze.way(leftLeft)
                            && maze.wall(Position2.sum(left, Position2.mul(dir, -1)))
                            && maze.wall(Position2.sum(left, dir))) {
                        maze.setWay(left);
                        wishToBreak = 0;
                    }
                }
            }

            if (maze.unchecked(right)) {
                if (blocked || wannaTurn()) {
                    guys.add(new MazeDiscoverer(maze, fillWia, pos, rightDir, wishToTurnCoeff, wishToStopCoeff, wishToBreakCoeff));
                    turned = true;
                }
                else {
                    maze.set(right, fillWia);
                }
            }
            else if (wishToBreak > 0 && wannaBreak()) {
                Position2 rightRight = Position2.sum(right, rightDir);
                if (!maze.border(rightRight)) {
                    if (maze.way(rightRight)
                            && maze.wall(Position2.sum(right, Position2.mul(dir, -1)))
                            && maze.wall(Position2.sum(right, dir))) {
                        maze.setWay(right);
                        wishToBreak = 0;
                    }
                }
            }

            maze.setWay(pos);

            if (turned) {
                wishToTurn = 0;
                canStop = true;
            }
            else {
                wishToStop += wishToStopCoeff;
                wishToTurn += wishToTurnCoeff;
                wishToBreak += wishToBreakCoeff;
            }

            if (canStop && !turned && wannaStop()) {
                maze.set(left, fillWia);
                maze.set(right, fillWia);
                maze.set(pos, fillWia);
                return true;
            }

            return false;
        }

        boolean wannaTurn() {
            return MathUtils.random(0, 99) < wishToTurn;
        }

        boolean wannaStop() {
            return MathUtils.random(0, 99) < wishToStop;
        }

        boolean wannaBreak() {
            return MathUtils.random(0, 99) < wishToBreak;
        }
    }

    class EndInMazeDiscoverer {
        Maze maze;
        Position2 pos;
        Position2 dir;
        int steps;
        int straitsSteps = 0;

        EndInMazeDiscoverer(Maze maze, Position2 pos, Position2 dir, int steps) {
            this.maze = maze;
            this.pos = new Position2(pos);
            this.dir = new Position2(dir);
            this.steps = steps;
        }

        boolean update(Array<EndInMazeDiscoverer> guys) {
            try {
                return makeStepToHappyFuture(guys);
            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Position of this not so great scientist: "
                        + pos.x + "x" + pos.y + " (" + maze.width + "x" + maze.height + ")"
                        + ", direction: (" + dir.x + ", " + dir.y + ")");
                throw e;
            }
        }

        private boolean makeStepToHappyFuture(Array<EndInMazeDiscoverer> guys) {
            pos.add(dir);
            steps++;

            if (pos.x < 0 || pos.x >= maze.width
                    || pos.y < 0 || pos.y >= maze.height) {
                return true;
            }

            Position2 left = Position2.left(dir);
            Position2 right = Position2.right(dir);

            if (!maze.wall(pos) && steps < paths[pos.x][pos.y] ) {
                paths[pos.x][pos.y] = steps;

                if (maze.wall(Position2.sum(pos, left))
                        && maze.wall(Position2.sum(pos,right))) {
                    if (5 == ++straitsSteps) {
                        Position2 roomPos = Position2.sum(pos, Position2.mul(dir, -2));

                        boolean horizontal = (dir.x != 0 ? true : false);
                        maze.setRoom(roomPos, horizontal);
                        generateRoom(pos, horizontal);
                        straitsSteps = 0;
                    }
                }
                else {
                    guys.add(new EndInMazeDiscoverer(maze, pos, left, steps));
                    guys.add(new EndInMazeDiscoverer(maze, pos, right, steps));
                    straitsSteps = 0;
                }

                return false;
            }
            else {
                return true;
            }
        }
    }
}

