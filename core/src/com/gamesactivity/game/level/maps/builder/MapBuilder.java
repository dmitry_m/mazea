package com.gamesactivity.game.level.maps.builder;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.maps.MainMap;
import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.maps.RoomMap;
import com.gamesactivity.game.level.maps.adjacentMaps.MainMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.adjacentMaps.RoomMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.filler.MainMapFiller;
import com.gamesactivity.game.level.maps.filler.MapFiller;
import com.gamesactivity.game.level.maps.filler.RoomMapFiller;
import com.gamesactivity.game.level.maps.renderer.MainMapRenderer;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.level.maps.renderer.RoomMapRenderer;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.renderer.InstanceRenderer;

/**
 * Created by dima on 13.10.2014.
 */
public class MapBuilder {
    private InstanceRenderer instanceRenderer;
    private InstanceFactory instanceFactory;

    public MapBuilder(InstanceRenderer instanceRenderer, InstanceFactory instanceFactory) {
        this.instanceRenderer = instanceRenderer;
        this.instanceFactory = instanceFactory;
    }

    public Map buildMain(Maze maze, MainMapAdjacentMapProvider mainMapAdjacentMapProvider) {
        MapContext mapContext = new MapContext(maze);

        MapFiller mapFiller = new MainMapFiller(instanceFactory);
        mapFiller.fillCells(mapContext);

        MapRenderer mapRenderer = new MainMapRenderer(mapContext, instanceRenderer, mainMapAdjacentMapProvider);

        return new MainMap(mapContext, mapRenderer, mainMapAdjacentMapProvider);
    }

    public Map buildRoom(Maze maze, Map parentMap) {
        MapContext mapContext = new MapContext(maze);

        MapFiller mapFiller = new RoomMapFiller(instanceFactory);
        mapFiller.fillCells(mapContext);

        RoomMapAdjacentMapProvider roomMapAdjacentMapProvider = new RoomMapAdjacentMapProvider(maze, parentMap);
        MapRenderer roomMapRenderer = new RoomMapRenderer(mapContext, instanceRenderer, roomMapAdjacentMapProvider);

        return new RoomMap(mapContext, roomMapRenderer, roomMapAdjacentMapProvider);
    }
}
