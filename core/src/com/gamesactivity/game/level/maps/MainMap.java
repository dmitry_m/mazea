package com.gamesactivity.game.level.maps;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.items.Item;
import com.gamesactivity.game.level.maps.adjacentMaps.MainMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.filler.MapFiller;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.renderer.controllers.CameraController;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;

import java.util.List;

public class MainMap extends Map {
    public MainMap(MapContext context, MapRenderer mapRenderer, MainMapAdjacentMapProvider mainMapAdjacentMapProvider) {
        super(context, mapRenderer, mainMapAdjacentMapProvider);
    }

    @Override
    protected void recalculateMapOffsetByLookIn(Position2 pos, Position2 dir) {
        context.mapOffset = Position2.sub(pos, context.pos);
    }

}

