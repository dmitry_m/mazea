package com.gamesactivity.game.level.maps;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.items.Cell;
import com.gamesactivity.game.utils.Position2;

import java.io.Serializable;

/**
 * Created by dima on 08.10.2014.
 */
public class MapContext implements Serializable {
    public Cell[][] cells;
    public Maze maze;
    public Position2 pos;
    public Position2 mapOffset;

    public MapContext(Maze maze) {
        this.maze = maze;
        this.pos = maze.start;
        this.mapOffset = new Position2();
        this.cells = new Cell[maze.width][maze.height];
    }
}