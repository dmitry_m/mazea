package com.gamesactivity.game.level.maps.filler;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.level.items.Cell;
import com.gamesactivity.game.level.items.ItemFactory;
import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 08.10.2014.
 */
public class RoomMapFiller implements MapFiller {
    MapContext context;
    ItemFactory itemFactory;
    InstanceFactory instanceFactory;

    public RoomMapFiller(InstanceFactory instanceFactory) {
        this.instanceFactory = instanceFactory;
        this.itemFactory = new ItemFactory(instanceFactory);
    }

    public void fillCells(MapContext context) {
        this.context = context;

        fillBorders(context.maze, context);
        fillOther(context);

        this.context = null;
    }

    private void fillOther(MapContext context) {
        for (int x = 1; x < context.maze.width; ++x) {
            for (int y = 0; y < context.maze.height; ++y) {
                initCellIfNeeded(x, y);
                fillCellWithItems(new Position2(x, y));
            }
        }
    }

    private void fillBorders(Maze maze, MapContext context) {
        getCell(0, 0).add(itemFactory.createWallItem("wall_corner_in", new Position2(0, 0), new Quaternion(0, 1, 0, Utils.ANGLE_DOWN + Utils.ANGLE_DELTA), Utils.DIR_DOWN));
        getCell(0, maze.height - 1).add(itemFactory.createWallItem("wall_corner_in", new Position2(0, maze.height - 1), new Quaternion(0, 1, 0, Utils.ANGLE_RIGHT + Utils.ANGLE_DELTA), Utils.DIR_RIGHT));
        getCell(maze.width - 1, 0).add(itemFactory.createWallItem("wall_corner_in", new Position2(0, maze.height - 1), new Quaternion(0, 1, 0, Utils.ANGLE_UP + Utils.ANGLE_DELTA), Utils.DIR_UP));
        getCell(maze.width - 1, maze.height - 1).add(itemFactory.createWallItem("wall_corner_in", new Position2(0, maze.height - 1), new Quaternion(0, 1, 0, Utils.ANGLE_LEFT + Utils.ANGLE_DELTA), Utils.DIR_LEFT));

        for (int x = 1; x < context.maze.width; ++x) {
            getCell(x, 0).add(itemFactory.createWallItem("wall", new Position2(x, 0), new Quaternion(0, 1, 0, Utils.ANGLE_DOWN), Utils.DIR_DOWN));
            getCell(x, maze.height - 1).add(itemFactory.createWallItem("wall", new Position2(x, maze.height - 1), new Quaternion(0, 1, 0, Utils.ANGLE_UP), Utils.DIR_UP));
        }
        for (int y = 0; y < context.maze.height; ++y) {
            getCell(0, y).add(itemFactory.createWallItem("wall", new Position2(0, y), new Quaternion(0, 1, 0, Utils.ANGLE_RIGHT), Utils.DIR_RIGHT));
            getCell(maze.width - 1, y).add(itemFactory.createWallItem("wall", new Position2(maze.width - 1, y), new Quaternion(0, 1, 0, Utils.ANGLE_LEFT), Utils.DIR_LEFT));
        }
    }

    private Cell initCellIfNeeded(int x, int y) {
        if (null == context.cells[x][y]) {
            context.cells[x][y] = new Cell();
        }
        return context.cells[x][y];
    }

    private Cell getCell(int x, int y) {
        return initCellIfNeeded(x, y);
    }

    private Cell getCell(Position2 pos) {
        return initCellIfNeeded(pos.x, pos.y);
    }

    private void fillCellWithItems(Position2 pos) {
        switch (context.maze.get(pos)) {
            case Maze.START:
            case Maze.END:
            case Maze.HORIZ:
            case Maze.VERT:
            case Maze.WAY:
                addWayCell(pos);
                break;

            case Maze.TABLE:
                addTableCell(pos);
                break;

            case Maze.WALL:
                break;

            default:
                break;
        }
    }

    private void addWayCell(Position2 pos) {
        getCell(pos).add(itemFactory.createFloorItem("floor", pos, new Vector3(), false));
    }

    private void addTableCell(Position2 pos) {
        getCell(pos).add(itemFactory.createFloorItem("floor", pos, new Vector3(0, 1, 0), true));
    }
}
