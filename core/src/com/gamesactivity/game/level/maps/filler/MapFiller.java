package com.gamesactivity.game.level.maps.filler;

import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.generator.Maze;

/**
 * Created by dima on 12.10.2014.
 */
public interface MapFiller {
    public void fillCells(MapContext context);
}
