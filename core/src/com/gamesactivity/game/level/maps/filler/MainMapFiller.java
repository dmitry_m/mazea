package com.gamesactivity.game.level.maps.filler;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.level.items.Cell;
import com.gamesactivity.game.level.items.ItemFactory;
import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 08.10.2014.
 */
public class MainMapFiller implements MapFiller {
    MapContext context;
    ItemFactory itemFactory;
    InstanceFactory instanceFactory;

    public MainMapFiller(InstanceFactory instanceFactory) {
        this.instanceFactory = instanceFactory;
        this.itemFactory = new ItemFactory(instanceFactory);
    }

    public void fillCells(MapContext context) {
        this.context = context;

        for (int x = 0; x < context.maze.width; ++x) {
            for (int y = 0; y < context.maze.height; ++y) {
                initCellIfNeeded(x, y);
                fillCellWithItems(new Position2(x, y));
            }
        }

        this.context = null;
    }

    private Cell initCellIfNeeded(int x, int y) {
        if (null == context.cells[x][y]) {
            context.cells[x][y] = new Cell();
        }
        return context.cells[x][y];
    }

    private Cell getCell(int x, int y) {
        return initCellIfNeeded(x, y);
    }

    private Cell getCell(Position2 pos) {
        return initCellIfNeeded(pos.x, pos.y);
    }

    private void fillCellWithItems(Position2 pos) {
        switch (context.maze.get(pos)) {
            case Maze.START:
            case Maze.END:
            case Maze.HORIZ:
            case Maze.VERT:
            case Maze.WAY:
                addWayCell(pos);
                break;

            case Maze.TABLE:
                addTableCell(pos);
                break;

            case Maze.WALL:
                break;

            default:
                break;
        }
    }

    private void addWayCell(Position2 pos) {
        getCell(pos).add(itemFactory.createFloorItem("floor", pos, new Vector3(), false));
        fillCellWithWalls(pos);
    }

    private void addTableCell(Position2 pos) {
        getCell(pos).add(itemFactory.createFloorItem("floor", pos, new Vector3(0, 1, 0), true));
    }

    private void fillCellWithWalls(Position2 pos) {
        performWallAnalysis(pos, Utils.DIR_RIGHT, Utils.ANGLE_LEFT);
        performWallAnalysis(pos, Utils.DIR_DOWN, Utils.ANGLE_UP);
        performWallAnalysis(pos, Utils.DIR_LEFT, Utils.ANGLE_RIGHT);
        performWallAnalysis(pos, Utils.DIR_UP, Utils.ANGLE_DOWN);
    }

    private void performWallAnalysis(Position2 pos, Position2 dir, float angle) {
        Position2 dirRight = Position2.right(dir);

        Position2 forward = Position2.sum(pos, dir);
        Position2 right = Position2.sum(pos, dirRight);
        Position2 forwardRight = Position2.sum(forward, dirRight);

        if (context.maze.valid(forward)) {
            if (context.maze.wall(forward)) {
                getCell(forward.x, forward.y).add(
                        itemFactory.createWallItem("wall", forward, new Quaternion(0, 1, 0, angle), dir));

                if (context.maze.valid(right) && context.maze.wall(right)
                        && context.maze.valid(forwardRight) && context.maze.wall(forwardRight)) {

                    getCell(forwardRight.x, forwardRight.y).add(
                            itemFactory.createWallItem(
                                    "wall_corner_in",
                                    forwardRight,
                                    new Quaternion(0, 1, 0, angle + Utils.ANGLE_DELTA),
                                    Position2.sum(dir, dirRight),
                                    Utils.OBSERVING_ANGLE + 45));
                }
            }
        }
    }
}
