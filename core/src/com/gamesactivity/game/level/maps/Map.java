package com.gamesactivity.game.level.maps;

import com.gamesactivity.game.level.items.Cell;
import com.gamesactivity.game.level.maps.adjacentMaps.AdjacentMapProvider;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

import java.io.Serializable;

/**
 * Created by dima on 28.09.2014.
 */

public abstract class Map implements Serializable {
    public MapContext context;
    private MapRenderer renderer;
    private AdjacentMapProvider adjacentMapProvider;

    public Map(MapContext context, MapRenderer mapRenderer, AdjacentMapProvider adjacentMapProvider) {
        this.context = context;
        this.renderer = mapRenderer;
        this.adjacentMapProvider = adjacentMapProvider;
    }

    protected abstract void recalculateMapOffsetByLookIn(Position2 pos, Position2 dir);

    public void render(float delta) {
        renderer.render(delta);
    }

    public final Map move(Position2 dir) {
        context.pos.add(dir);

        Map activeMap = this;
        if (adjacentMapProvider.isInAdjacentMap(context.pos, dir)) {
            activeMap = adjacentMapProvider.getAdjacentMap(context.pos);
            activeMap.moveIn();
            this.moveOut();
        }
        return activeMap;
    }

    public final boolean blocking(Position2 dir) {
        Position2 checkPos = Position2.sum(context.pos, dir);

        if (null != context.cells[checkPos.x][checkPos.y]) {
            return context.cells[checkPos.x][checkPos.y].blocking();
        }
        return true;
    }

    // look in - rendering from active map part of it's adjacent map
    public final void lookIn(float delta, Position2 pos, Position2 dir, int remain) {
        recalculateDueToLookIn(pos, dir);

        renderer.touch();
        renderer.renderInDirection(delta, Position2.sub(pos, context.mapOffset), Utils.DIR_LEFT, remain);
    }

    protected final void moveIn() {
        renderer.touch();

        recalculateDueToMoveIn();
    }

    private final void moveOut() {
        renderer.touch();

        recalculateDueToMoveOut();
    }

    private final void recalculateDueToLookIn(Position2 pos, Position2 dir) {
        recalculateMapOffsetByLookIn(pos, dir);
        recalculateCellsPositions();
    }

    private final void recalculateDueToMoveIn() {
        context.mapOffset = new Position2();
        recalculateCellsPositions();
    }

    private final void recalculateDueToMoveOut() {
        // nothing
    }

    private void recalculateCellsPositions() {
        for (int x = 0; x < context.maze.width; ++x) {
            for (int y = 0; y < context.maze.height; ++y) {
                Cell cell = context.cells[x][y];
                if (null != cell) {
                    cell.recalculatePositions(context.mapOffset);
                }
            }
        }
    }

    public MapRenderer getRenderer() {
        return renderer;
    }
}
