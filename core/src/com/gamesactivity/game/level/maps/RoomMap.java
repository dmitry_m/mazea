package com.gamesactivity.game.level.maps;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.items.Item;
import com.gamesactivity.game.level.maps.adjacentMaps.MainMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.adjacentMaps.RoomMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.filler.MapFiller;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.renderer.controllers.CameraController;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

import java.util.List;

/**
 * Created by dima on 12.10.2014.
 */
public class RoomMap extends Map {
    public RoomMap(MapContext context, MapRenderer mapRenderer, RoomMapAdjacentMapProvider roomMapAdjacentMapProvider) {
        super(context, mapRenderer, roomMapAdjacentMapProvider);
    }

    @Override
    protected void recalculateMapOffsetByLookIn(Position2 pos, Position2 dir) {
        Position2 enterPos;
        if (dir.equals(Utils.DIR_RIGHT)
                || dir.equals(Utils.DIR_UP)) {
            enterPos = context.maze.start;
        }
        else {
            enterPos = context.maze.end;
        }

        context.mapOffset = Position2.sub(pos, enterPos);
    }
}
