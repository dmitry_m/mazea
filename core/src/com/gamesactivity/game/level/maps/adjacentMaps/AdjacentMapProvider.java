package com.gamesactivity.game.level.maps.adjacentMaps;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.utils.Position2;

/**
 * Created by dima on 13.10.2014.
 */
public abstract class AdjacentMapProvider {
    protected Maze maze;

    AdjacentMapProvider(Maze maze) {
        this.maze = maze;
    }

    public abstract boolean isInAdjacentMap(Position2 pos, Position2 dir);

    public abstract Map getAdjacentMap(Position2 pos);

    public abstract MapRenderer getAdjacentMapRenderer(Position2 pos);
}
