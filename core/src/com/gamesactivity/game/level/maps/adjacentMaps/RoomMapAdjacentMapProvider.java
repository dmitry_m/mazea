package com.gamesactivity.game.level.maps.adjacentMaps;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.utils.Position2;

/**
 * Created by dima on 13.10.2014.
 */
public class RoomMapAdjacentMapProvider extends AdjacentMapProvider {
    private Map returnMap;

    public RoomMapAdjacentMapProvider(Maze maze, Map returnMap) {
        super(maze);
        this.returnMap = returnMap;
    }

    @Override
    public boolean isInAdjacentMap(Position2 pos, Position2 dir) {
        Position2 enterPos = Position2.sub(pos, dir);
        return !maze.valid(pos)
                && maze.start.equals(enterPos)
                && maze.end.equals(enterPos);
    }

    @Override
    public Map getAdjacentMap(Position2 pos) {
        return returnMap;
    }

    @Override
    public MapRenderer getAdjacentMapRenderer(Position2 pos) { return getAdjacentMap(pos).getRenderer(); }
}
