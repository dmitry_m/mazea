package com.gamesactivity.game.level.maps.adjacentMaps;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.renderer.MapRenderer;
import com.gamesactivity.game.utils.Position2;

/**
 * Created by dima on 13.10.2014.
 */
public class MainMapAdjacentMapProvider extends AdjacentMapProvider {
    private java.util.Map<Position2, Map> rooms;

    public MainMapAdjacentMapProvider(Maze maze) {
        super(maze);
    }

    @Override
    public boolean isInAdjacentMap(Position2 pos, Position2 dir) {
        return maze.room(pos);
    }

    @Override
    public Map getAdjacentMap(Position2 pos) {
        return rooms.get(pos);
    }

    @Override
    public MapRenderer getAdjacentMapRenderer(Position2 pos) { return getAdjacentMap(pos).getRenderer(); }

    public void setRooms(java.util.Map<Position2, Map> rooms) {
        this.rooms = rooms;
    }
}
