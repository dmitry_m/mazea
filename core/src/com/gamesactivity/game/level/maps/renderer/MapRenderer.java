package com.gamesactivity.game.level.maps.renderer;

import com.gamesactivity.game.level.items.Cell;
import com.gamesactivity.game.level.items.Item;
import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.maps.adjacentMaps.AdjacentMapProvider;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by dima on 13.10.2014.
 */



public abstract class MapRenderer {
    private InstanceRenderer instanceRenderer;
    private AdjacentMapProvider adjacentMapProvider;

    private MapContext mapContext;
    private RendererContext rendererContext;

    private List<Item> visibleItems = new LinkedList<Item>();
    private List<Item> renderedItems = new LinkedList<Item>();
    private List<MapRenderer> visibleAdjacentMapsRenderers = new LinkedList<MapRenderer>();
    private List<MapRenderer> renderedAdjacentMapsRenderers = new LinkedList<MapRenderer>();


    protected abstract void renderTurn(float delta, Position2 from, Position2 dir);

    protected abstract void itemFadeAction(Item it, boolean active, List<Item> renderedItems);

    public MapRenderer(MapContext context, InstanceRenderer instanceRenderer, AdjacentMapProvider adjacentMapProvider) {
        this.mapContext = context;
        this.instanceRenderer = instanceRenderer;
        this.adjacentMapProvider = adjacentMapProvider;
    }

    public void touch() {
        rendererContext.touch();
    }

    protected final void renderCell(float delta, Position2 pos, Position2 from) {
        if (!mapContext.maze.valid(pos)) {
            return;
        }

        if (null == mapContext.cells[pos.x][pos.y]) {
            return;
        }

        Cell cell = mapContext.cells[pos.x][pos.y];
        for (Item item : cell.items) {
            if (hideIfNotObservable(item)) {
                continue;
            }

            if (item.visible(from, pos)
                    && !item.visited) {
                item.renderFadeIn(delta, instanceRenderer);
                renderedItems.add(item);
            }
        }
    }

    protected final void renderLookIn(float delta, Position2 pos, Position2 dir, int remain) {
        this.lookInPos = pos;
        this.lookInDir = dir;
        this.remain = remain;

        renderItems(delta, false);
    }

    public final void render(float delta) {
        this.lookInPos = pos;
        this.lookInDir = dir;
        this.remain = remain;

        renderItems(delta, true);
        renderAdjacentMaps(delta);
    }

    private final void renderItems(float delta, boolean active) {
        if (!touched) {
            renderVisibleItems(delta);
        }
        else {
            if (active) {
                renderActive(delta);
            }
            else {
                renderInDirection(delta, Position2.sub(pos, mapContext.mapOffset), Utils.DIR_LEFT, remain);
            }

            renderOtherVisibleItems(delta, active);
        }
    }

    private final void renderActive(float delta, int visionRemain) {
        renderCell(delta, mapContext.pos, mapContext.pos);

        renderInDirection(delta, mapContext.pos, Utils.DIR_LEFT, visionRemain);
        renderInDirection(delta, mapContext.pos, Utils.DIR_RIGHT, visionRemain);
        renderInDirection(delta, mapContext.pos, Utils.DIR_DOWN, visionRemain);
        renderInDirection(delta, mapContext.pos, Utils.DIR_UP, visionRemain);
    }

    private boolean renderSide(float delta, Position2 pos, Position2 from, Position2 dir, boolean canTurnYet) {
        Position2 forward = Position2.sum(pos, dir);

        if (canTurnYet
                && mapContext.maze.valid(forward)
                && mapContext.maze.way(forward)) {
            renderTurn(delta, pos, dir);
            return true;
        }
        else {
            renderCell(delta, forward, from);
            return false;
        }
    }

    private final ERenderStep renderStep(float delta, Position2 pos, Position2 dir) {
        if (adjacentMapProvider.isInAdjacentMap(pos, dir)) {
            return ERenderStep.LookIn;
        }

        Position2 closestFromPos = Position2.sub(pos, dir);
        renderCell(delta, pos, closestFromPos);

        ERenderStep result = ERenderStep.Default;
        if (!(mapContext.maze.valid(pos) && mapContext.maze.transparent(pos))) {
            result = ERenderStep.ForceStop;
        }
        else {
            closestFromPos.add(dir);
        }

        boolean canTurnYet = (ERenderStep.ForceStop != result);
        if (renderSide(delta, pos, closestFromPos, Position2.left(dir), canTurnYet)
                | renderSide(delta, pos, closestFromPos, Position2.right(dir), canTurnYet)) {
            // render next if at least one turn is rendered, because border of turn  (far from hero) is at next step
            if (ERenderStep.ForceStop != result) {
                return ERenderStep.ForceNext;
            }
        }

        return result;
    }

    public final void renderInDirection(float delta, Position2 pos, Position2 dir, int remain) {
        ERenderStep result = renderStep(delta, pos, dir);
        switch (result) {
            case Default:
                if (0 < remain) {
                    renderInDirection(delta, Position2.sum(pos, dir), dir, remain - 1);
                }
                break;

            case ForceNext:
                renderInDirection(delta, Position2.sum(pos, dir), dir, remain - 1);
                break;

            case ForceStop:
                break;

            case LookIn:
                Map adjacentMap = adjacentMapProvider.getAdjacentMap(pos);
                adjacentMap.lookIn(delta, pos, dir, remain - 1);
                renderedAdjacentMapsRenderers.add(adjacentMapProvider.getAdjacentMapRenderer(pos));
                break;

            default:
                break;
        }
    }

    private final void renderVisibleItems(float delta) {
        for (Item item : visibleItems) {
            item.renderFadeIn(delta, instanceRenderer);
        }
    }

    private final void renderOtherVisibleItems(float delta, boolean active) {
        for (Item item : visibleItems) {
            if (hideIfNotObservable(item)) {
                continue;
            }

            if (!item.visited) {
                if (item.renderFadeOut(delta, instanceRenderer)) {
                    itemFadeAction(item, active, renderedItems);
                }
                else {
                    renderedItems.add(item);
                }
            }
        }
        visibleItems = renderedItems;
        renderedItems = new LinkedList<Item>();
    }

    private final boolean hideIfNotObservable(Item item) {
        return false;
    }
}
