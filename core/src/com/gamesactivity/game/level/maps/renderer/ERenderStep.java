package com.gamesactivity.game.level.maps.renderer;

/**
 * Created by dima on 12.10.2014.
 */

enum ERenderStep {
    Default,
    ForceStop,
    ForceNext,
    LookIn
}
