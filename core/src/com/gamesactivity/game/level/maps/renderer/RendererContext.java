package com.gamesactivity.game.level.maps.renderer;

import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 22.10.2014.
 */
public class RendererContext {
    private boolean active;
    private int remain;
    private Position2 lookInPos;
    private Position2 lookInDir;
    private boolean touched;

    public RendererContext() {
        resetContext();
    }

    public void initLookInContext(Position2 pos, Position2 dir, int remain) {
        this.active = false;
        this.lookInPos = pos;
        this.lookInDir = dir;
        this.remain = remain;
    }

    public void initContext() {
        this.active = true;
        this.remain = Utils.VISION_REMAIN_DEFAULT;
    }

    public void closeContext() {
        resetContext();
    }

    private void resetContext() {
        this.remain = Utils.VISION_REMAIN_DEFAULT;
        this.lookInPos = null;
        this.lookInDir = null;
        this.touched = false;
    }

    public boolean isActive() {
        return active;
    }

    public int getRemain() {
        return remain;
    }

    public Position2 getLookInPos() {
        return lookInPos;
    }

    public Position2 getLookInDir() {
        return lookInDir;
    }

    public void isTouched() {
        touched = true;
    }

    public boolean touched() {
        return touched;
    }
}
