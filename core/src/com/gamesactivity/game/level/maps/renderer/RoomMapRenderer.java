package com.gamesactivity.game.level.maps.renderer;

import com.gamesactivity.game.level.items.Item;
import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.maps.adjacentMaps.RoomMapAdjacentMapProvider;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;

import java.util.List;

/**
 * Created by dima on 13.10.2014.
 */
public class RoomMapRenderer extends MapRenderer {
    public RoomMapRenderer(MapContext mapContext, InstanceRenderer instanceRenderer, RoomMapAdjacentMapProvider roomMapAdjacentMapProvider) {
        super(mapContext, instanceRenderer, roomMapAdjacentMapProvider);
    }

    @Override
    protected void renderTurn(float delta, Position2 from, Position2 dir) {
        // in room turns should be hidden
    }

    @Override
    protected void itemFadeAction(Item item, boolean active, List<Item> renderedItems) {
        if (active) {
            renderedItems.add(item);
        }
    }
}
