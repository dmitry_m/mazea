package com.gamesactivity.game.level.maps.renderer;

import com.gamesactivity.game.level.items.Item;
import com.gamesactivity.game.level.maps.MapContext;
import com.gamesactivity.game.level.maps.adjacentMaps.MainMapAdjacentMapProvider;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;

import java.util.List;

/**
 * Created by dima on 13.10.2014.
 */
public class MainMapRenderer extends MapRenderer {
    public MainMapRenderer(MapContext context, InstanceRenderer instanceRenderer, MainMapAdjacentMapProvider mainMapAdjacentMapProvider) {
        super(context, instanceRenderer, mainMapAdjacentMapProvider);
    }

    @Override
    protected void renderTurn(float delta, Position2 from, Position2 dir) {
        Position2 forward = Position2.sum(from, dir);
        Position2 forwardLeft = Position2.sum(forward, Position2.left(dir));
        Position2 forwardRight = Position2.sum(forward, Position2.right(dir));

        renderCell(delta, forward, from);
        renderCell(delta, forwardLeft, forward);
        renderCell(delta, forwardRight, forward);
    }

    @Override
    protected void itemFadeAction(Item item, boolean active, List<Item> renderedItems) {
        renderedItems.add(item);
    }
}
