package com.gamesactivity.game.level;

import com.gamesactivity.game.level.generator.Generator;
import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.adjacentMaps.AdjacentMapProvider;
import com.gamesactivity.game.level.maps.adjacentMaps.MainMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.adjacentMaps.RoomMapAdjacentMapProvider;
import com.gamesactivity.game.level.maps.builder.MapBuilder;
import com.gamesactivity.game.level.maps.filler.MainMapFiller;
import com.gamesactivity.game.level.maps.filler.MapFiller;
import com.gamesactivity.game.level.maps.filler.RoomMapFiller;
import com.gamesactivity.game.level.maps.MainMap;
import com.gamesactivity.game.level.maps.RoomMap;
import com.gamesactivity.game.level.util.MapAndRooms;
import com.gamesactivity.game.level.util.MazeAndRooms;
import com.gamesactivity.game.renderer.controllers.CameraController;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.renderer.SpecialRenderable;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

import java.io.*;
import java.util.HashMap;

/**
 * Created by dima on 25.09.2014.
 */
public class Level implements SpecialRenderable {
    private MazeAndRooms mazeAndRooms;
    private MapAndRooms mapAndRooms;
    private Map map;
    private boolean loadFromFile = false;

    // it is strictly recommended not to pollute this area wia bad ideas.
    public void init(MapBuilder mapBuilder) {
        if (loadFromFile) {
            read();
        }
        else {
            Generator generator = new Generator();
            this.mazeAndRooms = generator.generate(20, true);
            initMapAndRooms(mapBuilder);
            this.map = mapAndRooms.map;
        }

        move(Utils.DIR_NONE);
    }

    private void initMapAndRooms(MapBuilder mapBuilder) {
        MainMapAdjacentMapProvider mainMapAdjacentMapProvider = new MainMapAdjacentMapProvider(mazeAndRooms.maze);
        Map mainMap = mapBuilder.buildMain(mazeAndRooms.maze, mainMapAdjacentMapProvider);

        java.util.Map<Position2, Map> roomMaps = new HashMap<Position2, Map>();
        for (int x = 0; x < mazeAndRooms.maze.width; ++x) {
            for (int y = 0; y < mazeAndRooms.maze.height; ++y) {
                if (mazeAndRooms.maze.room(x, y)) {
                    Position2 key = new Position2(x, y);

                    Map roomMap = mapBuilder.buildRoom(mazeAndRooms.rooms.get(key), mainMap);

                    roomMaps.put(key, roomMap);
                }
            }
        }
        mainMapAdjacentMapProvider.setRooms(roomMaps);
        this.mapAndRooms = new MapAndRooms(mainMap, roomMaps);
    }

    public void read() {
        try {
            FileInputStream mapFis = new FileInputStream("map.map");
            ObjectInputStream mapOin = new ObjectInputStream(mapFis);
            this.mapAndRooms = (MapAndRooms) mapOin.readObject();

            FileInputStream mazeFis = new FileInputStream("map.maze");
            ObjectInputStream mazeOin = new ObjectInputStream(mazeFis);
            this.mazeAndRooms = (MazeAndRooms) mazeOin.readObject();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
        catch (ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    public void save() {
        String mapFileName = "map.map";
        String mazeFileName = "map.maze";

        try {
            FileOutputStream mapFos = new FileOutputStream(mapFileName);
            ObjectOutputStream mapOos = new ObjectOutputStream(mapFos);
            mapOos.writeObject(mapAndRooms);
            mapOos.flush();
            mapOos.close();

            FileOutputStream mazeFos = new FileOutputStream(mazeFileName);
            ObjectOutputStream mazeOos = new ObjectOutputStream(mazeFos);
            mazeOos.writeObject(mazeAndRooms);
            mazeOos.flush();
            mazeOos.close();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void render(float delta, InstanceRenderer renderer) {
        map.render(delta);
    }

    public Position2 getPos() {
        return map.context.pos;
    }

    public boolean blocking(Position2 dir) {
        return map.blocking(dir);
    }

    public void move(Position2 dir) {
        map.move(dir);
    }
}
