package com.gamesactivity.game.level.util;

import com.gamesactivity.game.level.generator.Maze;
import com.gamesactivity.game.utils.Position2;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by dima on 28.09.2014.
 */
public class MazeAndRooms implements Serializable {
    public Maze maze;
    public Map<Position2, Maze> rooms;

    public MazeAndRooms(Maze maze, Map<Position2, Maze> rooms) {
        this.maze = maze;
        this.rooms = rooms;
    }
}
