package com.gamesactivity.game.level.util;

import com.gamesactivity.game.level.maps.Map;
import com.gamesactivity.game.level.maps.RoomMap;
import com.gamesactivity.game.utils.Position2;

import java.io.Serializable;

/**
 * Created by dima on 28.09.2014.
 */
public class MapAndRooms implements Serializable {
    public Map map;
    public java.util.Map<Position2, Map> rooms;

    public MapAndRooms(Map map, java.util.Map<Position2, Map> rooms) {
        this.map = map;
        this.rooms = rooms;
    }

}
