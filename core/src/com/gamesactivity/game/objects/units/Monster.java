package com.gamesactivity.game.objects.units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.level.Level;
import com.gamesactivity.game.renderer.InstanceFactory;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.utils.Position2;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 25.09.2014.
 */
public class Monster extends Unit {
    private InstanceFactory instanceFactory;

    private boolean moving;
    private Position2 cell;
    private Position2 nextCell;
    private Vector3 startPos;
    private Vector3 endPos;
    private Vector3 acc;
    private Position2 movingDir;
    private int keyForKeepMoving;

    public Monster(Level map, InstanceFactory instanceFactory) {
        super(map);
        this.moving = false;
        this.instanceFactory = instanceFactory;
    }

    public void init() {
        ModelInstance monster = instanceFactory.createInstance("monster");
        super.init(monster);

        cell = new Position2(map.getPos());
        pos = Utils.conv2Real(cell.toVector3());
    }

    @Override
    public void update(float delta) {
        super.update(delta);

        if (moving) {
            Vector3 vel = new Vector3(acc);
            vel.scl(delta);
            pos.add(vel);

            boolean runEnough = startPos.dst(pos) > startPos.dst(endPos);
            if (runEnough) {
                processMoving(keepMoving());
            }
        }
        else {
            processInput();
        }

        updatePosition(delta);
    }

    @Override
    public void render(float delta, InstanceRenderer renderer) {
        super.render(delta, renderer);
    }

    private void processInput() {
        Position2 dir = null;
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            dir = Utils.DIR_LEFT;
            keyForKeepMoving = Input.Keys.LEFT;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            dir = Utils.DIR_DOWN;
            keyForKeepMoving = Input.Keys.UP;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            dir = Utils.DIR_RIGHT;
            keyForKeepMoving = Input.Keys.RIGHT;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            dir = Utils.DIR_UP;
            keyForKeepMoving = Input.Keys.DOWN;
        }

        movingDir = dir;
        recalculateMoving();
    }

    private void recalculateMoving() {
        if (null == movingDir || map.blocking(movingDir)) {
            return;
        }

        map.move(movingDir);

        nextCell = new Position2(map.getPos());
        startPos = Utils.conv2Real(cell.toVector3());
        endPos = Utils.conv2Real(nextCell.toVector3());
        acc = new Vector3(endPos).sub(startPos).scl(2.5f);

        moving = true;
    }

    private void endMoving() {
        cell = nextCell;

        nextCell = null;
        startPos = null;
        endPos = null;
        acc = null;

        moving = false;
    }

    private void processMoving(boolean keepMoving) {
        if (!keepMoving) {
            pos = new Vector3(endPos);
        }

        endMoving();

        if (keepMoving) {
            recalculateMoving();
        }
    }

    private boolean keepMoving() {
        return Gdx.input.isKeyPressed(keyForKeepMoving);
    }

    private void updatePosition(float delta) {
        instance.transform.setTranslation(pos);
    }
}
