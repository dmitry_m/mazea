package com.gamesactivity.game.objects.units;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.level.Level;
import com.gamesactivity.game.renderer.InstanceRenderer;
import com.gamesactivity.game.renderer.SpecialRenderable;

/**
 * Created by dima on 25.09.2014.
 */
public class Unit implements Updatable, SpecialRenderable {
    protected Level map;
    protected ModelInstance instance;
    protected Vector3 pos;

    Unit(Level map) {
        this.map = map;
    }

    public void init(ModelInstance instance) {
        this.instance = instance;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void render(float delta, InstanceRenderer renderer) {
        renderer.render(instance);
    }

    public ModelInstance getInstance() {
        return instance;
    }

    public Vector3 getPos() {
        return pos;
    }
}
