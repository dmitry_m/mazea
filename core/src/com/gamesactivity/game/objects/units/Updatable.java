package com.gamesactivity.game.objects.units;

/**
 * Created by dima on 08.10.2014.
 */
public interface Updatable {
    public void update(float delta);
}
