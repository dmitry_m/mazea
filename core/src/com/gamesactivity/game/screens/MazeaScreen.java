
package com.gamesactivity.game.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Game;

public abstract class MazeaScreen implements Screen {
	Game game;

	public MazeaScreen(Game game) {
		this.game = game;
	}

    public void doneLoading() {
    }

	@Override
	public void resize (int width, int height) {
	}

	@Override
	public void show () {
	}

	@Override
	public void hide () {
	}

	@Override
	public void pause () {
	}

	@Override
	public void resume () {
	}

	@Override
	public void dispose () {
	}
}
