
package com.gamesactivity.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.game.level.Level;
import com.gamesactivity.game.level.maps.builder.MapBuilder;
import com.gamesactivity.game.objects.units.Monster;
import com.gamesactivity.game.objects.units.Updatable;
import com.gamesactivity.game.renderer.*;
import com.gamesactivity.game.renderer.controllers.CameraController;
import com.gamesactivity.game.renderer.controllers.LightController;

public class GameScreen extends MazeaScreen {
    private CameraController cameraController;
    private LightController lightController;
    private Renderer renderer;
    private Array<Updatable> updatables = new Array<Updatable>();
    private Level map;
    private Monster monster;

	public GameScreen (Game game) {
		super(game);
        this.renderer = new Renderer(this);
        this.map = new Level();
        this.monster = new Monster(map, renderer.getInstancesFactory());
        this.cameraController = new CameraController(monster);
        this.lightController = new LightController(monster);
	}

    public void doneLoading() {
        renderer.init(cameraController, lightController);
        map.init(new MapBuilder(renderer.getInstanceRenderer(), renderer.getMapInstancesFactory()));
        monster.init();

        renderer.addSpecialRenderable(map);
        updatables.add(monster);
        updatables.add(cameraController);
        updatables.add(lightController);
    }

    public void render (float delta) {
        update(delta);

        renderer.render(delta);
    }

    private void update(float delta) {
        for (Updatable updatable : updatables) {
            updatable.update(delta);
        }
    }
}
