package com.gamesactivity.game.renderer;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 28.09.2014.
 */
public class InstanceFactory {
    private Array<ModelInstance> store;
    private AssetManager assets;

    InstanceFactory(Array<ModelInstance> store, AssetManager assets) {
        this.store = store;
        this.assets = assets;
    }

    public void clear() {
        store.clear();
    }

    public ModelInstance createInstance(String name) {
        if (null == name) {
            return null;
        }

        Model model = assets.get(Utils.model(name), Model.class);
        ModelInstance instance = new ModelInstance(model);
        store.add(instance);
        return instance;
    }

    public ModelInstance createInstance(String name, Vector3 position) {
        ModelInstance instance = createInstance(name);
        if (null != instance) {
            instance.transform.translate(position);
        }
        return instance;
    }

    public ModelInstance createInstance(String name, Vector3 position, Quaternion quaternion) {
        ModelInstance instance = createInstance(name, position);
        if (null != instance) {
            instance.transform.rotate(Vector3.Y, quaternion.w);
        }
        return instance;
    }
}
