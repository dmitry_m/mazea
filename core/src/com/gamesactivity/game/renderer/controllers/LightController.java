package com.gamesactivity.game.renderer.controllers;

import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.objects.units.Unit;
import com.gamesactivity.game.objects.units.Updatable;

/**
 * Created by dima on 08.10.2014.
 */
public class LightController implements Updatable {
    Unit target;
    PointLight light;
    private final Vector3 lightOffset = new Vector3(0, 5, 0);

    public LightController(Unit target) {
        this.target = target;
        PointLight light = new PointLight().set(0f, 0f, 0f, -10f, -0.8f, -0.2f, 0);
        //DirectionalLight light = new DirectionalLight().set(0.1f, 0.1f, 0.1f, -10f, -0.8f, -0.2f);
        this.light = light;
    }

    @Override
    public void update(float delta) {
        light.position.set(new Vector3(target.getPos()).add(lightOffset));
    }

    public PointLight getLight() {
        return light;
    }
}
