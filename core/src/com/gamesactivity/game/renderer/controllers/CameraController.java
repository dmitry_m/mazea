package com.gamesactivity.game.renderer.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.gamesactivity.game.level.items.Item;
import com.gamesactivity.game.objects.units.Unit;
import com.gamesactivity.game.objects.units.Updatable;

/**
 * Created by dima on 08.10.2014.
 */
public class CameraController implements Updatable {
    private PerspectiveCamera camera;
    private Unit target;
    private final Vector3 cameraOffset = new Vector3(-7, 15, 0);
    private float angle = 45;

    public CameraController(Unit target) {
        this.target = target;
        adjustCamera();
    }

    private void adjustCamera() {
        camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 0f);
        camera.lookAt(0, 0, 0);
        camera.near = 1f;
        camera.far = 300f;
        camera.update();
    }

    private void processInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.I)) {
            cameraOffset.scl(1.0f/1.2f);
        }
        else if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {
            cameraOffset.scl(1.2f);
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.K)) {
            angle--;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.L)) {
            angle++;
        }

        angle = ((angle + 360) % 360);
    }

    @Override
    public void update(float delta) {
        processInput();
        updateCamera(delta);
    }

    private void updateCamera(float delta) {
        Vector3 offset = new Vector3(cameraOffset);
        offset.rotate(Vector3.Y, angle);

        camera.position.set(new Vector3(target.getPos()).add(offset));
        camera.direction.set(new Vector3(target.getPos()).sub(camera.position));
        camera.update();
    }

    public boolean isItemObservable(Item item) {
        return true;
        /*
        if (0 == item.context.areas.x && 0 == item.context.areas.y) {
            return true;
        }

        final float itemAngle = Utils.getDirAngle(item.context.areas);
        final float limit = item.context.observeAngle + 1f;
        return Math.abs(itemAngle - angle) <= limit
                || Math.abs(itemAngle + 360f - angle) <= limit
                || Math.abs(itemAngle - angle - 360f) <= limit;
         */
    }

    public PerspectiveCamera getCamera() {
        return camera;
    }
}
