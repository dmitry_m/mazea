package com.gamesactivity.game.renderer;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.gamesactivity.game.renderer.controllers.CameraController;
import com.gamesactivity.game.utils.Utils;

/**
 * Created by dima on 08.10.2014.
 */
public class InstanceRenderer {
    private ModelBatch modelBatch;
    private Environment environment;
    private CameraController cameraController;

    public InstanceRenderer(ModelBatch modelBatch, Environment environment, CameraController cameraController) {
        this.modelBatch = modelBatch;
        this.environment = environment;
        this.cameraController = cameraController;
    }

    public void render(ModelInstance instance, float brightness) {
        if (null != instance) {

            float level = Utils.MAX_BRIGHTNESS * brightness;
            environment.set(new ColorAttribute(ColorAttribute.AmbientLight, level, level, level, level));

            modelBatch.begin(cameraController.getCamera());
            modelBatch.render(instance, environment);
            modelBatch.end();
        }
    }

    public void render(ModelInstance instance) {
        render(instance, Utils.MAX_BRIGHTNESS);
    }
}
