
package com.gamesactivity.game.renderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.game.renderer.controllers.CameraController;
import com.gamesactivity.game.renderer.controllers.LightController;
import com.gamesactivity.game.screens.MazeaScreen;
import com.gamesactivity.game.utils.Utils;

public class Renderer {
    private InstanceFactory instancesFactory;
    private InstanceFactory mapInstancesFactory;
    private InstanceRenderer instancesRenderer;

    private CameraController cameraController;
    private LightController lightController;
    private MazeaScreen screen;
    private boolean loading;

    private final ModelBatch modelBatch = new ModelBatch();
    private final Environment environment = new Environment();
    private final AssetManager assets = new AssetManager();
    private final Array<SpecialRenderable> mazeRenderables = new Array<SpecialRenderable>();
    private final Array<ModelInstance> instances = new Array<ModelInstance>();
    private final Array<ModelInstance> mapInstances = new Array<ModelInstance>();

    private ColorAttribute attribute;

    public Renderer(MazeaScreen screen) {
        this.screen = screen;
        this.loading = true;

        initAssets();
        initEnviroment();
    }

    public void init(CameraController cameraController, LightController lightController) {
        this.cameraController = cameraController;
        this.lightController = lightController;
        environment.add(lightController.getLight());
    }

    private void initAssets() {
        assets.load(Utils.model("monster"), Model.class);
        assets.load(Utils.model("floor"), Model.class);
        assets.load(Utils.model("wall"), Model.class);
        assets.load(Utils.model("wall_corner_in"), Model.class);
        assets.load(Utils.model("wall_dark_left"), Model.class);
        assets.load(Utils.model("wall_dark_right"), Model.class);
    }

    private void initEnviroment() {
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, Utils.MAX_BRIGHTNESS, Utils.MAX_BRIGHTNESS, Utils.MAX_BRIGHTNESS, Utils.MAX_BRIGHTNESS));
    }

    public void render(float delta) {
        if (!loaded()
                || null == cameraController) {
            return;
        }

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        renderSpecial(delta);
        renderUsual(delta);
    }

    private void renderSpecial(float delta) {
        for (SpecialRenderable each : mazeRenderables) {
            each.render(delta, getInstanceRenderer());

            environment.set(new ColorAttribute(ColorAttribute.AmbientLight, Utils.MAX_BRIGHTNESS, Utils.MAX_BRIGHTNESS, Utils.MAX_BRIGHTNESS, Utils.MAX_BRIGHTNESS));
        }
    }

    private void renderUsual(float delta) {
        modelBatch.begin(cameraController.getCamera());
        modelBatch.render(instances, environment);
        //modelBatch.render(mapInstances, environment);
        modelBatch.end();
    }

    private void doneLoading() {
        screen.doneLoading();
    }

    public void dispose () {
        modelBatch.dispose();
        assets.dispose();
    }

    private boolean loaded() {
        if (!loading) {
            return true;
        }

        if (assets.update()) {
            doneLoading();
            loading = false;
            return true;
        }
        else {
            return false;
        }
    }
    public SpecialRenderable addSpecialRenderable(SpecialRenderable mazeRenderable) {
        mazeRenderables.add(mazeRenderable);
        return mazeRenderable;
    }

    public InstanceFactory getInstancesFactory() {
        if (null == instancesFactory) {
            instancesFactory = new InstanceFactory(instances, assets);
        }
        return instancesFactory;
    }

    public InstanceFactory getMapInstancesFactory() {
        if (null == mapInstancesFactory) {
            mapInstancesFactory = new InstanceFactory(mapInstances, assets);
        }
        return mapInstancesFactory;
    }

    public InstanceRenderer getInstanceRenderer() {
        if (null == instancesRenderer) {
            instancesRenderer = new InstanceRenderer(modelBatch, environment, cameraController);
        }
        return instancesRenderer;
    }
}
