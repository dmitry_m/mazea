package com.gamesactivity.game.renderer;

/**
 * Created by dima on 08.10.2014.
 */
public interface SpecialRenderable {
    public void render(float delta, InstanceRenderer renderer);
}
