package com.gamesactivity.game;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.Game;

public class mazea extends Game {
    public Array<ModelInstance> instances = new Array<ModelInstance>();
    private AnimationController controller;

    @Override
    public void create () {

        //Gdx.app.exit();

        Manager.init(this);
        Manager.restart();
    }
}
